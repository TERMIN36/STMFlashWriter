/*!
  @file FlashWriter.hpp
  @brief Заголовочный файл для модуля библиотеки STMFlashWriter
  @defgroup FlashWriter FlashWriter
  @brief Модуль для работы с Flash памятью. Поддерживает семейство STM32F103
  @author termin36@yandex.ru
  @{
*/
#ifndef _FLASH_WRITER_
#define _FLASH_WRITER_
#include "stm32f1xx_hal.h"
#include "stdint.h"

#ifndef __cplusplus
	#error A compiler with C ++ support is needed
#endif
#ifndef STM32F1
	#warning Warning, the class was not tested on this STM32 family
#endif


/// Адрес первой страницы Flash памяти
#define FLASH_WRITER_START_ADDRESS_FIRST_PAGE          0x08000000
/// Размер одной страницы Flash памяти
#define FLASH_WRITER_ONE_PAGE_SIZE                     0x0400

/**
  @brief Класс по работе с Flash памятью. Принадлежит модулю FlashWriter. Поддерживает семейство STM32F103
  @warning Класс не реализует блокировку прерываний и остановку работы операционной системы если таковая присутствует. Пользователь должен сам об этом заботиться при операции
    записи в Flash память.
  @version 1.0.0
*/
class FlashPage {
  public:
    /** @brief Конструктор класса */
    FlashPage(uint8_t numberPage);
    /// Метод для чтения данных из Flash памяти в массив
    void readPageFromFlash(uint32_t startAddress, uint16_t* data, uint16_t size);
    /// Метод для записи данных из массива в Flash память
    void writeBlockPageInFlash(uint32_t startAddress, uint16_t* data, uint16_t size);
    /// Метод смены номера страницы
    void setNumberPage(uint8_t numberPage);
    /// Метод запроса номера страницы
    uint8_t getNumberPage(void);
  protected:
    /// Метод снимающий блокировку на запись в Flash память
    void unLockFlash();
    /// Метод устанавливающий блокировку на запись в Flash память
    void lockFlash();
  private:
    uint8_t numberPage;
};
/** @} */
#endif
