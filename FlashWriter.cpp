/*! \mainpage Заглавная
 *
 * \section intro_sec Инструкция по применению
 *
 * Скопируйте файлы исходных кодов в директорию с проектом и импортируйте файлы в среду. Так же необходимо сделать include заголовочного файла.
 * После этого необходимо создать экземпляр класса FlashPage.
*/
/*!
@file FlashWriter.cpp
@author termin36@yandex.ru
@brief Исполнительный файл для модуля библиотеки STMFlashWriter
*/
#include "stm32f1xx_hal.h"
#include "FlashWriter.hpp"
#include "stdint.h"

// Конструктор класса для создания объекта
/**
  @param numberPage номер страницы памяти с которой будет работать экземпляр
  @see setNumberPage, getNumberPage
*/
FlashPage::FlashPage(uint8_t numberPage) {
  this->numberPage = numberPage;
}
// Метод для чтения данных из Flash памяти в массив
/**
  @param[in] startAddress адрес страницы памяти с которой будет работать метод
  @param[out] *data указатель на массив в который будет происходить запись данных из Flash памяти
  @param[in] size количество байт для чтения
  @see writeBlockPageInFlash
*/
void FlashPage::readPageFromFlash(uint32_t startAddress, uint16_t* data, uint16_t size) {
  uint16_t d = 0;
  uint32_t pageAddress = numberPage * FLASH_WRITER_ONE_PAGE_SIZE + FLASH_WRITER_START_ADDRESS_FIRST_PAGE;
  for(uint32_t i = pageAddress + startAddress * 2; i < pageAddress + startAddress + size * 2; i += 2){
    *(data + d++) = *((uint32_t*) i);
  }
}
// Метод для записи данных из массива в Flash память
/**
  @param[in] startAddress адрес страницы памяти с которой будет работать метод
  @param[out] *data указатель на массив из которого будет происходить чтение данных и запись в Flash память
  @param[in] size количество байт для записи
  @see writeBlockPageInFlash
*/
void FlashPage::writeBlockPageInFlash(uint32_t startAddress, uint16_t* data, uint16_t size) {
  unLockFlash();
  uint16_t d = 0;
  uint32_t pageAddress = numberPage * FLASH_WRITER_ONE_PAGE_SIZE + FLASH_WRITER_START_ADDRESS_FIRST_PAGE;
  for(uint32_t i = pageAddress + startAddress * 2; i < pageAddress + startAddress * 2 + size * 2; i +=2){
    HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, i, *(data + d++));
    // не менять задержку! только полное ожидание камня, запрещено уходить в этот момент в прерывания или еще куда либо
    HAL_Delay(100);
  }
  lockFlash();
}
/// Метод снимающий блокировку на запись в Flash память
void FlashPage::unLockFlash() {
  HAL_FLASH_Unlock();
  uint32_t pageAddress = numberPage * FLASH_WRITER_ONE_PAGE_SIZE + FLASH_WRITER_START_ADDRESS_FIRST_PAGE;
  FLASH_PageErase(pageAddress);
  CLEAR_BIT(FLASH->CR, FLASH_CR_PER);
}
/// Метод устанавливающий блокировку на запись в Flash память
void FlashPage::lockFlash() {
  HAL_FLASH_Lock();
}
// Метод смены номера страницы
/**
  @param[in] numberPage номер страницы с которой надо работать
  @see getNumberPage
*/
void FlashPage::setNumberPage(uint8_t numberPage) {
  this -> numberPage = numberPage;
}
//Метод запроса номера страницы
/**
  @return номер текущей страницы
  @see setNumberPage
*/
uint8_t FlashPage::getNumberPage(void) {
  return this -> numberPage;
}
